package com.example.kyurim.bnr_runtracker_gps_sqlite_concurrency_googmapsapi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RunFragment extends Fragment {

  private BroadcastReceiver mLocationReceiver = new LocationReceiver() {
    @Override
    protected void onLocationReceived(Context context, Location loc) {
      mLastLocation = loc;
      if (isVisible())
        updateUI();
    }

    @Override
    protected void onProviderEnabledChanged(boolean enabled) {
      int toastText = enabled ? R.string.gps_enabled : R.string.gps_disabled;
//      int testText2;
//      if (enabled) {
//        testText2 = R.string.gps_enabled;
//      } else {
//        testText2 = R.string.gps_enabled;
//      }
      Toast.makeText(getActivity(), toastText, Toast.LENGTH_SHORT).show();
    }
  };

  private Run mRun;
  private Location mLastLocation;
  private RunManager mRunManger;
  private Button mStartButton, mStopButton;
  private TextView mStartedTextView, mLatitudeTextView, mLongditudeTextView, mAltitudeTextView, mDurationTextView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);
    mRunManger = RunManager.get(getActivity());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_run, container, false);

    mStartedTextView = (TextView) view.findViewById(R.id.run_startedTextView);
    mLatitudeTextView = (TextView) view.findViewById(R.id.run_latitudeTextView);
    mLongditudeTextView = (TextView) view.findViewById(R.id.run_longitudeTextView);
    mAltitudeTextView = (TextView) view.findViewById(R.id.run_altitudeTextView);
    mDurationTextView = (TextView) view.findViewById(R.id.run_durationTextView);

    mStartButton = (Button) view.findViewById(R.id.run_startButton);
    mStartButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mRunManger.startLocationUpdates();
        mRun = new Run();
        updateUI();
      }
    });

    mStopButton = (Button) view.findViewById(R.id.run_stopButton);
    mStopButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mRunManger.stopLocationUpdates();
        updateUI();
      }
    });

    updateUI();

    return view;
  }

  @Override
  public void onStart() {
    super.onStart();
    getActivity().registerReceiver(mLocationReceiver, new IntentFilter(RunManager.ACTION_LOCATION));
  }

  @Override
  public void onStop() {
    getActivity().unregisterReceiver(mLocationReceiver);
    super.onStop();
  }

  private void updateUI() {
    boolean started = mRunManger.isTrackingRun();

    if (mRun != null)
      mStartedTextView.setText(mRun.getStartDate().toString());

    int durationSeconds = 0;
    if (mRun != null && mLastLocation != null) {
      durationSeconds = mRun.getDurationSeconds(mLastLocation.getTime());
      mLatitudeTextView.setText(Double.toString(mLastLocation.getLatitude()));
      mLongditudeTextView.setText(Double.toString(mLastLocation.getLongitude()));
      mAltitudeTextView.setText(Double.toString(mLastLocation.getAltitude()));
    }
    mDurationTextView.setText(Run.formatDuration(durationSeconds));

    mStartButton.setEnabled(!started);
    mStopButton.setEnabled(started);
  }
}
